

SRC_URI = "git://gitlab.com/prpl-foundation/components/ambiorix/applications/amxb-inspect.git;protocol=https;nobranch=1"
S = "${WORKDIR}/git"
inherit pkgconfig config-amx

SUMMARY = "Ambiorix Backend inspector/validation tool"
LICENSE += "SAH & BSD-2-Clause-Patent"

LIC_FILES_CHKSUM = " \
                    file://LICENSE.SAH;md5=a0f6bf5b78959aa070b853b0ad21d9a2 \
                    file://LICENSE.BSD;md5=6bb6609ec7c25caf8b7b0eb6ed4480cf \
                    "

COMPONENT = "amxb-inspect"


DEPENDS += "libamxb"

RDEPENDS:${PN} += "libamxb"

FILES:${PN} += "${BINDIR}/${COMPONENT}"
