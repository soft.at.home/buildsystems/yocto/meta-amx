

SRC_URI = "git://gitlab.com/prpl-foundation/components/ambiorix/applications/amxrt.git;protocol=https;nobranch=1"
SRCREV = "v2.2.4"

S = "${WORKDIR}/git"

inherit pkgconfig config-amx

SUMMARY = "Data model runtime"
LICENSE += "SAH & BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

COMPONENT = "amxrt"



export CONFIG_SAH_SERVICES_AMXRT

CONFIG_SAH_SERVICES_AMXRT ??= "y"

SAH_CONFIG += " \
                CONFIG_SAH_SERVICES_AMXRT \
                "

DEPENDS += "libamxc"
DEPENDS += "libamxj"
DEPENDS += "libamxp"
DEPENDS += "libamxd"
DEPENDS += "libamxo"
DEPENDS += "libamxb"
DEPENDS += "libevent"
DEPENDS += "libcap-ng"
DEPENDS += "libamxrt"

RDEPENDS:${PN} += "libamxc"
RDEPENDS:${PN} += "libamxj"
RDEPENDS:${PN} += "libamxp"
RDEPENDS:${PN} += "libamxd"
RDEPENDS:${PN} += "libamxo"
RDEPENDS:${PN} += "libamxb"
RDEPENDS:${PN} += "libevent"
RDEPENDS:${PN} += "libcap-ng"
RDEPENDS:${PN} += "libamxrt"

inherit update-rc.d
INITSCRIPT_NAME = "amx-shutdown-wait"

INITSCRIPT_PARAMS = "start 90 2 3 4 5 . stop 90 0 1 6 ."

EXTRA_OEMAKE += "DEST=${D} \
                 PREFIX=${prefix} \
                 LIBDIR=${libdir} \
                 BINDIR=${bindir} \
                 INCLUDEDIR=${includedir} \
                 "

FILES:${PN} += "${BINDIR}/${COMPONENT}"
FILES:${PN} += "/usr/lib/amx/scripts/amx_init_functions.sh"
FILES:${PN} += "${INITDIR}/amx-shutdown-wait"
