

SRC_URI = "git://gitlab.com/prpl-foundation/components/ambiorix/libraries/libamxc.git;protocol=https;nobranch=1"
SRCREV = "sah-next_v2.4.0"

S = "${WORKDIR}/git"

inherit pkgconfig config-amx

SUMMARY = "library providing generic reusable data containers"
LICENSE += "SAH & BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=f8eaa5ae328592b1346db42ce2936d90"

COMPONENT = "libamxc"



export CONFIG_SAH_LIB_AMXC

CONFIG_SAH_LIB_AMXC ??= "y"

SAH_CONFIG += " \
                CONFIG_SAH_LIB_AMXC \
                "



EXTRA_OEMAKE += "DEST=${D} \
                 PREFIX=${prefix} \
                 LIBDIR=${libdir} \
                 BINDIR=${bindir} \
                 INCLUDEDIR=${includedir} \
                 "

FILES:${PN}-dev += "${INCLUDEDIR}/amxc/*.h"
FILES:${PN}-staticdev += "${LIBDIR}/${COMPONENT}.a"
FILES:${PN} += "${LIBDIR}/${COMPONENT}${SOLIBS}"
FILES:${PN}-dev += "${LIBDIR}/${COMPONENT}${SOLIBSDEV}"
