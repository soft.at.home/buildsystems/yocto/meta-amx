

SRC_URI = "git://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_pcb.git;protocol=https;nobranch=1"
SRCREV = "v3.14.3"

S = "${WORKDIR}/git"

inherit pkgconfig config-amx

SUMMARY = "PCB backend implementation for amxb"
LICENSE += "SAH & BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

COMPONENT = "mod-amxb-pcb"



export CONFIG_SAH_MOD_AMXB_PCB
export CONFIG_SAH_MOD_AMXB_PCB_ORDER

CONFIG_SAH_MOD_AMXB_PCB ??= "y"
CONFIG_SAH_MOD_AMXB_PCB_ORDER ??= "10-"

SAH_CONFIG += " \
                CONFIG_SAH_MOD_AMXB_PCB \
                CONFIG_SAH_MOD_AMXB_PCB_ORDER \
                "

DEPENDS += "libamxc"
DEPENDS += "libamxj"
DEPENDS += "libamxp"
DEPENDS += "libamxd"
DEPENDS += "libamxb"
DEPENDS += "libamxa"
DEPENDS += "libpcb"
DEPENDS += "libusermngt"
DEPENDS += "yajl"

RDEPENDS:${PN} += "libamxc"
RDEPENDS:${PN} += "libamxj"
RDEPENDS:${PN} += "libamxp"
RDEPENDS:${PN} += "libamxd"
RDEPENDS:${PN} += "libamxb"
RDEPENDS:${PN} += "libpcb"
RDEPENDS:${PN} += "libamxa"
RDEPENDS:${PN} += "libusermngt"
RDEPENDS:${PN} += "yajl"

EXTRA_OEMAKE += "DEST=${D} \
                 PREFIX=${prefix} \
                 LIBDIR=${libdir} \
                 BINDIR=${bindir} \
                 INCLUDEDIR=${includedir} \
                 "

FILES:${PN} += "${BINDIR}/mods/amxb/${CONFIG_SAH_MOD_AMXB_PCB_ORDER}${COMPONENT}.so"
