DESCRIPTION = "package group for the Ambiorix with lua"

LICENSE = "MIT"

SECTION = "packagegroup"

inherit packagegroup

RDEPENDS:${PN} = " \
    packagegroup-amx-core \
    lua \
    lua-amx \
    mod-lua-amx \
    "
