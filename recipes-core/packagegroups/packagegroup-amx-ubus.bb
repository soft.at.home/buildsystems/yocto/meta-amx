# Packagegroups ubus sysbus

SUMMARY = "Enable ambiorix and the ubus sysbus"
PR = "r1"

inherit packagegroup

DEPENDS += "libubox"
RDEPENDS:${PN} = "\
    packagegroup-ubus \
    mod-amxb-ubus \
    "
