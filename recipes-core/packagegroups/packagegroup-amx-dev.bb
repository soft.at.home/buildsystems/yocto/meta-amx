DESCRIPTION = "package group for the Ambiorix project"

LICENSE = "MIT"

SECTION = "packagegroup"

inherit packagegroup

RDEPENDS:${PN} = " \
    packagegroup-amx-core \
    amxb-inspect \
    amxo-cg \
    amxo-xml-to \
    mod-dm-cli \
    "
