DESCRIPTION = "package group for the Ambiorix with lua"

LICENSE = "MIT"

SECTION = "packagegroup"

inherit packagegroup

RDEPENDS:${PN} = " \
    python-amx \
    python3-core \
    python3-pip \
    python3-async \
    "
