#
# Package Versions for PrplOS 3.2
#
include prplos-3.1.inc

# libamxb in prplos 3.2 is actually v4.11.1 with backported fixes
PREFERRED_VERSION_libamxb ?= "v4.11.5"
