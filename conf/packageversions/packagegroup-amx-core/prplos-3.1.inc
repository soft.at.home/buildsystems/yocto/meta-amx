#
# Package Versions for PrplOS 3.1
#
PREFERRED_VERSION_amx-cli ?= "v0.5.1"
PREFERRED_VERSION_amxrt ?= "v2.2.0"
PREFERRED_VERSION_libamxa ?= "v0.11.1"
PREFERRED_VERSION_libamxb ?= "v4.11.1"
PREFERRED_VERSION_libamxc ?= "v2.1.0"
PREFERRED_VERSION_libamxd ?= "v6.5.5"
PREFERRED_VERSION_libamxj ?= "v1.0.3"
PREFERRED_VERSION_libamxm ?= "v0.1.0"
PREFERRED_VERSION_libamxo ?= "v5.0.2"
PREFERRED_VERSION_libamxp ?= "v2.3.0"
PREFERRED_VERSION_libamxrt ?= "v0.6.3"
PREFERRED_VERSION_libamxs ?= "v0.6.4"
PREFERRED_VERSION_libamxt ?= "v1.0.0"
PREFERRED_VERSION_mod-ba-cli ?= "v0.13.0"
PREFERRED_VERSION_mod-sahtrace ?= "v1.2.2"
PREFERRED_VERSION_uriparser ?= "0.9.3"
