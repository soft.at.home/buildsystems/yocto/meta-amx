#
# Package Versions for PrplOS 3.1
#
PREFERRED_VERSION_amxb-inspect ?= "v1.2.1"
PREFERRED_VERSION_amxo-cg ?= "v1.5.4"
PREFERRED_VERSION_amxo-xml-to ?= "v0.2.7"
PREFERRED_VERSION_mod-dm-cli ?= "v0.2.6"
